import React, { Component } from 'react';
import { groupBy } from 'lodash';
import { format } from 'date-fns';
import './App.css';

export default class App extends Component {
  state = {
    url: '',
    days: [],
  };

  async componentDidMount() {
    const user = await fetch('/api/users/me', {
      credentials: 'include',
    }).then(res => res.json());

    if (user.error) {
      sessionStorage.clear();
      this.setState({
        url: '',
        days: [],
      });

      return;
    }

    sessionStorage.setItem('user', JSON.stringify(user));

    const links = await fetch('/api/links', {
      credentials: 'include',
    }).then(res => res.json());

    if (links.error) {
      console.error(links.error);
      return;
    }

    const linksWithDay = links.map(link => ({
      ...link,
      day: link.createdAt.split('T').shift(),
      sortKey: link.createdAt.split('T').shift().replace(/-/g, ''),
    }));

    const groupByDay = groupBy(linksWithDay, 'day');

    const days = Object.keys(groupByDay)
      .map(day => ({
        day,
        sortKey: day.replace(/-/g, ''),
        links: groupByDay[day],
      }))
      .sort((a, b) => b.sortKey - a.sortKey);

    this.setState({
      days,
      user,
    });
  }

  handleBurgerMenu = (evt, data) => {
    const target = document.getElementById(evt.currentTarget.dataset.target);
    evt.target.classList.toggle('is-active');
    target.classList.toggle('is-active');
  };

  handleAddLinkSubmit = async evt => {
    evt.preventDefault();

    const link = await fetch('/api/links', {
      headers: {
        'content-type': 'application/json',
      },
      method: 'post',
      body: JSON.stringify({ url: this.state.url }),
      credentials: 'include',
    }).then(res => res.json());

    if (link.error) {
      console.error(link);
      return;
    }

    const [dateDay] = link.createdAt.split('T');
    const day = this.state.days.find(d => d.day === dateDay) || {
      day: dateDay,
      links: [],
      sortKey: dateDay.replace(/-/g, ''),
    };

    day.links.unshift(link);

    this.setState({
      days: [...this.state.days.filter(d => d.day !== dateDay), day].sort(
        (a, b) => b.sortKey - a.sortKey,
      ),
      url: '',
    });
  };

  handleDeleteLink = async (evt, dayDateString, linkId) => {
    evt.preventDefault();

    const link = await fetch(`/api/links/${linkId}`, {
      headers: {
        'content-type': 'application/json',
      },
      credentials: 'include',
      method: 'delete',
    }).then(res => res.json());

    if (link.error) {
      console.error(link.error);
      return;
    }

    const day = this.state.days.find(d => d.day === dayDateString);
    day.links = day.links.filter(link => link._id !== linkId);

    this.setState({
      days: [...this.state.days.filter(d => d.day !== dayDateString), day].sort(
        (a, b) => b.sortKey - a.sortKey,
      ),
    });
  };

  handleUrlChange = evt => {
    this.setState({
      url: evt.target.value,
    });
  };

  renderLinks() {
    return this.state.days.map(day =>
      <div key={day.day} className="content">
        <h2>
          {day.day}
          {/* {format(day.date, 'YYYY-MM-DD', { locale: navigator.language })} */}
        </h2>
        <hr />
        {day.links.map(link =>
          <div key={link._id} className="content">
            <div className="columns is-mobile is-1">
              <div className="column">
                <a href={link.url} target="_blank">
                  {link.title}
                </a>
              </div>

              <div className="column is-narrow link-tag-controls is-0">
                <a id="link-tag">
                  <span className="icon" style={{ color: '#aaa' }}>
                    <i className="fa fa-tag" />
                  </span>
                </a>
              </div>

              <div className="column is-narrow link-delete-controls">
                <a
                  id="link-delete"
                  onClick={e => {
                    [...e.currentTarget.parentElement.children].forEach(el =>
                      el.classList.toggle('is-hidden'),
                    );
                  }}
                >
                  <span className="icon" style={{ color: '#aaa' }}>
                    <i className="fa fa-trash" />
                  </span>
                </a>
                <a
                  className="is-hidden"
                  onClick={() => {
                    console.log(link._id);
                  }}
                  id="link-delete-cancel"
                  style={{ color: '#aaa' }}
                >
                  <span
                    className="icon"
                    id="link-delete-ok"
                    style={{ color: '#aaa' }}
                    onClick={e => this.handleDeleteLink(e, day.day, link._id)}
                  >
                    <i className="fa fa-check" />
                  </span>
                </a>
                <a
                  className="is-hidden"
                  onClick={e => {
                    [...e.currentTarget.parentElement.children].forEach(el =>
                      el.classList.toggle('is-hidden'),
                    );
                  }}
                  id="link-delete-cancel"
                  style={{ color: '#aaa' }}
                >
                  <span className="icon">
                    <i className="fa fa-close" />
                  </span>
                </a>
              </div>
            </div>

            {link.tags && link.tags.length > 0
              ? <div className="columns">
                  <div className="column">
                    <div className="tags">
                      {link.tags.map((tag, idx) =>
                        <span key={idx} className="tag">
                          {tag}
                        </span>,
                      )}
                    </div>
                  </div>
                </div>
              : ''}
          </div>,
        )}
      </div>,
    );
  }

  render() {
    return (
      <div>
        <div className="modal " id="delete-link-modal">
          <div className="modal-background" />
          <div className="modal-card">
            <div className="field is-grouped">
              <p className="control">
                <a className="button is-primary">Save changes</a>
              </p>
              <p className="control">
                <a className="button">Cancel</a>
              </p>
              <p className="control">
                <a className="button is-danger">Delete post</a>
              </p>
            </div>
          </div>
        </div>

        <nav className="navbar">
          <div className="navbar-brand">
            <a className="navbar-item" href="http://bulma.io">
              <span className="icon" style={{ color: '#333' }}>
                <i className="fa fa-link" />
              </span>
              <span style={{ paddingLeft: '1rem' }}>Daily Links</span>
            </a>

            <div
              className="navbar-burger burger"
              data-target="navMenu"
              onClick={this.handleBurgerMenu}
            >
              <span />
              <span />
              <span />
            </div>
          </div>
          <div className="navbar-menu" id="navMenu">
            <div className="navbar-end">
              <div className="navbar-item">
                <p>
                  {this.state.user
                    ? <a className="navbar-item" href="/auth/logout">
                        <span className="icon" style={{ color: '#333' }}>
                          <i className="fa fa-sign-out" />
                        </span>{' '}
                        Logout
                      </a>
                    : <a className="navbar-item" href="/auth/github">
                        <span className="icon" style={{ color: '#333' }}>
                          <i className="fa fa-github" />
                        </span>{' '}
                        Login
                      </a>}
                </p>
              </div>
            </div>
          </div>
        </nav>

        <section className="hero is-primary has-text-centered">
          <div className="hero-body">
            <div className="container">
              <h1 className="title">Daily Links</h1>
              <h2 className="subtitle">Track read articles & watched videos</h2>
            </div>
          </div>
        </section>

        <section className="section">
          <div className="container">
            <div className="field has-addons">
              <div className="control is-expanded">
                <input
                  className="input is-medium"
                  type="text"
                  value={this.state.url}
                  placeholder="https://"
                  required
                  onChange={this.handleUrlChange}
                />
              </div>
              <div className="control" onClick={this.handleAddLinkSubmit}>
                <a className="button is-medium is-info">Add Link</a>
              </div>
            </div>
          </div>
        </section>

        <section className="section" style={{ paddingTop: 0 }}>
          <div className="container">
            {this.state.days.length ? this.renderLinks() : 'There are no links'}
          </div>
        </section>
      </div>
    );
  }
}
