import express from 'express';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import helmet from 'helmet';
import boom from 'boom';
import morgan from 'morgan';
import jwt from 'express-jwt';
import * as models from 'src/models';
import routes from 'src/routes';
import authentication from 'src/authentication';

const app = express();

if (process.env.NODE_ENV !== 'test') app.use(morgan('dev'));
app.use(helmet());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());

authentication({ app, User: models.User });

// Populate `req.user` from JWT if available
app.use(
  jwt({
    secret: process.env.APP_SECRET,
    credentialsRequired: false,
    getToken(req) {
      return (req.cookies && req.cookies['daily-log-jwt']) || null;
    },
  }),
);

routes({ app, models });

// eslint-disable-next-line no-unused-vars
app.use((err, req, res, next) => {
  if (err.isBoom) {
    res.status(err.output.statusCode).json(err.output.payload);
    return;
  }

  // express-jwt 401 error
  if (err.name === 'UnauthorizedError') {
    res.status(401).json(boom.unauthorized().output.payload);
    return;
  }

  res.status(500).json(boom.internal().output.payload);
});

export default app;
