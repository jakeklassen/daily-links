import mongoose from 'mongoose';
import logger from 'winston';
import app from 'app';

mongoose.Promise = global.Promise;

const server = app.listen(process.env.PORT, err => {
  if (err) logger.error(err);
  else logger.info(`Server listening`);
});

mongoose.connect(process.env.MONGO_URL, {
  useMongoClient: true,
});

process.on('unhandledRejection', (reason, p) =>
  logger.error('Unhandled Rejection at: Promise ', p, reason),
);

process.on('SIGINT', async () => {
  await mongoose.disconnect();
  server.close();
});
