import mongoose from 'mongoose';

export const SCHEMA_VERSION = 1;

const UserSchema = new mongoose.Schema(
  {
    name: String,
    googleId: { type: String, unique: true },
    githubId: { type: Number, unique: true },
    schemaVersion: {
      type: Number,
      required: true,
      default: SCHEMA_VERSION,
    },
  },
  { timestamps: true },
);

UserSchema.statics.SCHEMA_VERSION = SCHEMA_VERSION;

export default mongoose.model('User', UserSchema);
