import mongoose from 'mongoose';
import { isURL } from 'validator';

export const SCHEMA_VERSION = 1;

const LinkSchema = new mongoose.Schema(
  {
    user: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      index: true,
      ref: 'User',
    },
    url: {
      type: String,
      validate: {
        validator(v) {
          return isURL(v);
        },
        message: '{VALUE} is not a valid URL',
      },
      required: true,
    },
    title: {
      type: String,
      required: true,
      index: true,
    },
    tags: {
      type: [String],
      sparse: true,
    },
    schemaVersion: {
      type: Number,
      required: true,
      default: SCHEMA_VERSION,
    },
  },
  { timestamps: true },
);

LinkSchema.statics.SCHEMA_VERSION = SCHEMA_VERSION;

export default mongoose.model('Link', LinkSchema);
