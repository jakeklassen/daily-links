/**
 * Parse IncomingMessage stream for HTML <head> contents
 * @param {http.IncomingMessage} readable
 */
const readHTMLHead = readable =>
  new Promise((resolve, reject) => {
    if (!readable) {
      reject(new Error('Readable required'));
      return;
    }

    // Error non-OK status
    if (readable.statusCode < 200 || readable.statusCode > 299) {
      reject(readable.error);
      return;
    }

    let html = '';

    readable
      .on('error', reject)
      .on('data', chunk => {
        html += chunk.toString();

        // Look for the closing head tag
        if (chunk.toString().indexOf('</head>') !== -1) {
          readable.destroy();
        }
      })
      .on('end', () => {
        const headStartIdx = html.indexOf('<head');
        const headEndIdx = html.indexOf('</head>') + 7;

        resolve(html.substr(headStartIdx, headEndIdx - headStartIdx));
      })
      .on('close', () => {
        const headStartIdx = html.indexOf('<head');
        const headEndIdx = html.indexOf('</head>') + 7;

        resolve(html.substr(headStartIdx, headEndIdx - headStartIdx));
      });
  });

export default readHTMLHead;
