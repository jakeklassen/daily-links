import jsonwebtoken from 'jsonwebtoken';
import { pick } from 'lodash';

const createUserJWT = (user, secret, opts = {}) =>
  jsonwebtoken.sign(
    {
      id: user.id,
      ...pick(user, ['name', 'githubId', 'googleId', 'createdAt', 'updatedAt']),
    },
    secret,
    {
      expiresIn: '30d',
      ...opts,
    },
  );

export default createUserJWT;
