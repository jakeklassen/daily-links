import boom from 'boom';

export default (req, res, next) => {
  if (req.link.user.toString() !== req.user.id) {
    next(boom.unauthorized());
  } else {
    next();
  }
};
