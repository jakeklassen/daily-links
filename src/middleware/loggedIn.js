import boom from 'boom';

const loggedIn = (req, res, next) => {
  if (!req.user) {
    next(boom.unauthorized());
    return;
  }

  next();
};

export default loggedIn;
