import boom from 'boom';
import { Types } from 'mongoose';

export default function populateLinkFactory(Link) {
  return async (req, res, next, id) => {
    try {
      Types.ObjectId(id);
    } catch (error) {
      next(boom.badData());
    }

    try {
      const link = await Link.findById(id);

      if (!link) {
        next(boom.notFound());
        return;
      }

      req.link = link;
      next();
    } catch (error) {
      next(boom.serverUnavailable());
    }
  };
}
