import passport from 'passport';
import { Strategy as GithubStrategy } from 'passport-github2';

export default ({ app, User }) => {
  passport.use(
    new GithubStrategy(
      {
        clientID: process.env.GITHUB_CLIENT_ID,
        clientSecret: process.env.GITHUB_CLIENT_SECRET,
        callbackURL: `${process.env.HOST}:${process.env
          .PORT}/auth/github/callback`,
      },
      async (accessToken, refreshToken, profile, done) => {
        let user = await User.findOne({ githubId: profile.id });

        if (!user) {
          user = await User.create({
            name: profile.displayName || profile.username,
            githubId: profile.id,
          });
        }

        done(null, user);
      },
    ),
  );

  app.use(passport.initialize());
};
