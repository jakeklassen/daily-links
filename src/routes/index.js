import express from 'express';
import loggedIn from 'src/middleware/loggedIn';
import populateLink from 'src/middleware/populateLink';
import auth from 'src/routes/auth';
import links from 'src/routes/links';
import users from 'src/routes/users';
import { Link } from 'src/models';

export default ({ app }) => {
  const apiRouter = express.Router();

  auth(app);

  // API routes
  const usersRouter = express.Router();
  users({ router: usersRouter });

  const linksRouter = express.Router();
  linksRouter.param('id', populateLink(Link));

  links({ router: linksRouter });

  apiRouter.use('/users', usersRouter);
  apiRouter.use('/links', linksRouter);

  app.use('/api', loggedIn, apiRouter);
};
