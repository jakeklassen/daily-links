import boom from 'boom';

export default ({ router }) => {
  router.get('/:id', async (req, res, next) => {
    if (req.params.id === 'me') {
      res.json(req.user);
      return;
    }

    next(boom.forbidden());
  });
};
