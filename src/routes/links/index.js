import boom from 'boom';
import request from 'request';
import cheerio from 'cheerio';
import restrictToLinkOwner from 'src/middleware/restrictToLinkOwner';
import { Link } from 'src/models';
import readHTMLHead from 'src/lib/url/readHTMLHead';

export default ({ router }) => {
  router.post('/', async (req, res, next) => {
    try {
      const { url } = req.body;
      let dom;

      try {
        const stream = await new Promise((resolve, reject) => {
          request(url)
            .on('error', reject)
            .on('response', resolve);
        });

        dom = await readHTMLHead(stream);
      } catch (error) {
        next(boom.badRequest());
        return;
      }

      const $ = cheerio.load(dom);

      const link = await Link.create({
        url,
        user: req.user.id,
        title: $('head title').text(),
      });

      res.status(201).json(link);
    } catch (error) {
      next(error);
    }
  });

  router.patch('/:id', restrictToLinkOwner, async (req, res, next) => {
    try {
      await Link.findByIdAndUpdate(req.link.id, req.body);

      res.end();
    } catch (error) {
      next(boom.internal());
    }
  });

  router.get('/', async (req, res, next) => {
    try {
      const links = await Link.find({ user: req.user.id }, null, {
        sort: { createdAt: -1 },
        limit: 50,
      });

      res.json(links);
    } catch (error) {
      next(boom.internal());
    }
  });

  router.delete('/:id', restrictToLinkOwner, async (req, res, next) => {
    try {
      await Link.findByIdAndRemove(req.link.id);

      res.json(req.link);
    } catch (error) {
      next(boom.internal());
    }
  });
};
