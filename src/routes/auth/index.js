import passport from 'passport';
import logger from 'winston';
import createUserJWT from 'lib/auth/createUserJWT';

export default router => {
  router.get(
    '/auth/github',
    async (req, res, next) => {
      await new Promise(resolve => setTimeout(() => resolve(), 5000));
      next();
    },
    passport.authenticate('github', { session: false, scope: ['user:email'] }),
  );

  router.get('/auth/github/callback', (req, res, next) => {
    passport.authenticate('github', { session: false }, (err, user) => {
      if (err) {
        logger.error(err);
        next(err);
        return;
      }

      if (!user) {
        res.status(401).json({ message: 'auth failed' });
        return;
      }

      req.login(user, { session: false }, loginErr => {
        if (loginErr) {
          logger.error(loginErr);
          next(loginErr);
          return;
        }

        const token = createUserJWT(user, process.env.APP_SECRET, {
          expiresIn: '30d',
        });

        res.cookie('daily-log-jwt', token, {
          secure: process.env.NODE_ENV === 'production',
          httpOnly: true,
        });

        res.redirect(process.env.CLIENT_HOST);
      });
    })(req, res, next);
  });

  router.get('/auth/logout', (req, res) => {
    req.logout();
    res.clearCookie('daily-log-jwt', { httpOnly: true });

    res.redirect(process.env.CLIENT_HOST);
  });
};
