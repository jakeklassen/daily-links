import { Factory } from 'rosie';
import Chance from 'chance';
import mongoose from 'mongoose';
import { Link } from 'src/models';

const chance = new Chance();

export default new Factory()
  .attrs({
    _id: () => mongoose.Types.ObjectId(),
    user: () => mongoose.Types.ObjectId().toString(),
    url: () => chance.url(),
    title: () => chance.sentence({ words: 4 }),
    createdAt: () => new Date().toISOString(),
    updatedAt: () => new Date().toISOString(),
    schemaVersion: Link.SCHEMA_VERSION,
  })
  .attr('id', ['_id'], _id => _id.toString());
