import logger from 'winston';
import shortid from 'shortid';
import mongoose from 'mongoose';

mongoose.Promise = global.Promise;

process.on('unhandledRejection', (reason, p) =>
  logger.error('Unhandled Rejection at: Promise ', p, reason),
);

const { MONGO_URL } = process.env;

export const scaffold = () => {
  // Generate test db address
  const dbAddress = `${MONGO_URL}/daily-log-test-${shortid.generate()}`;

  mongoose.connect(dbAddress, {
    useMongoClient: true,
    config: {
      autoIndex: false,
    },
  });

  return {
    mongoose,
  };
};

export const teardown = async mongooseInstance =>
  mongooseInstance.connection.close();
