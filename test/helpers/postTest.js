import 'dotenv/config';
import { MongoClient } from 'mongodb';
import logger from 'winston';

// This script will clean up the test databases

// Add to package.json scripts to run
// "posttest": "cross-env NODE_ENV=test node -r babel-register ./test/helpers/postTest.js"

const MONGO_URL = process.env.GITLAB_CI
  ? 'mongodb://mongo'
  : 'mongodb://localhost';

MongoClient.connect(MONGO_URL, async (err, db) => {
  try {
    if (err) throw err;

    // Load all databases
    const { databases } = await db.admin().listDatabases();
    // Find those matching `daily-log-test-[-xxx]` pattern
    const toBeDropped = databases.filter(({ name }) =>
      name.match(/^daily-log-test(-.*)?$/),
    );

    // Connect to all databases
    const connections = await Promise.all(
      toBeDropped.map(({ name }) =>
        MongoClient.connect(`${MONGO_URL}/${name}`),
      ),
    );

    // Drop all databases
    await Promise.all(connections.map(connection => connection.dropDatabase()));

    // Close all connections
    await Promise.all(connections.map(connection => connection.close()));
    await db.close();

    logger.info('Test databases dropped');
  } catch (error) {
    logger.error(error);

    await db.close();
  }
});
