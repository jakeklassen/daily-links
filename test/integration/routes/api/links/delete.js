import test from 'ava';
import request from 'supertest';
import sinon from 'sinon';
import app from 'src/app';
import { User, Link } from 'src/models';
import createUserJWT from 'lib/auth/createUserJWT';
import { Types } from 'mongoose';
import { scaffold, teardown } from 'test/helpers';

const { mongoose } = scaffold();

test.after.always(() => teardown(mongoose));

test('guests cannot delete links', async t => {
  await request(app)
    .delete('/api/links')
    .expect(401)
    .then((res, err) => {
      t.falsy(err);

      t.is(res.body.statusCode, 401);
      t.is(res.body.error, 'Unauthorized');
    });
});

test('should 422 on bad link object id', async t => {
  const user = await User.create({
    name: 'user',
    githubId: Date.now(),
  });

  const jwt = createUserJWT(user, process.env.APP_SECRET);

  await request(app)
    .delete('/api/links/does-not-exist')
    .set('cookie', `daily-log-jwt=${jwt}`)
    .expect(422)
    .then((res, err) => {
      t.falsy(err);

      t.is(res.body.statusCode, 422);
      t.truthy(res.body.error);
    });
});

test('should 404 on missing link', async t => {
  const user = await User.create({
    name: 'user',
    githubId: Date.now(),
  });

  const jwt = createUserJWT(user, process.env.APP_SECRET);

  await request(app)
    .delete(`/api/links/${Types.ObjectId().toString()}`)
    .set('cookie', `daily-log-jwt=${jwt}`)
    .expect(404)
    .then((res, err) => {
      t.falsy(err);

      t.is(res.body.statusCode, 404);
      t.truthy(res.body.error);
    });
});

test.serial('should handle delete server error', async t => {
  const user = await User.create({
    name: 'user',
    githubId: Date.now(),
  });

  const link = await Link.create({
    user: user.id,
    title: 'JS Testing With Ava',
    url: 'https://medium.com/js-testing-with-ava',
    tags: ['javascript', 'testing', 'ava'],
  });

  const jwt = createUserJWT(user, process.env.APP_SECRET);

  sinon.stub(Link, 'findByIdAndRemove').rejects(new Error('internal'));

  await request(app)
    .delete(`/api/links/${link.id}`)
    .set('cookie', `daily-log-jwt=${jwt}`)
    .expect(500)
    .then((res, err) => {
      t.truthy(res.body.error);
      t.falsy(err);

      Link.findByIdAndRemove.restore();
    });
});

test('users can delete their own links', async t => {
  const user = await User.create({
    name: 'user',
    githubId: Date.now(),
  });

  const link = await Link.create({
    user: user.id,
    title: 'JS Testing With Ava',
    url: 'https://medium.com/js-testing-with-ava',
    tags: ['javascript', 'testing', 'ava'],
  });

  const jwt = createUserJWT(user, process.env.APP_SECRET);

  await request(app)
    .delete(`/api/links/${link.id}`)
    .set('cookie', `daily-log-jwt=${jwt}`)
    .expect(200)
    .then((res, err) => {
      t.truthy(res.body);
      t.falsy(err);
    });
});

test('users cannot delete another users links', async t => {
  const user1 = await User.create({
    name: 'user',
    githubId: Date.now(),
  });

  const user1Link = await Link.create({
    user: user1.id,
    title: 'JS Testing With Ava',
    url: 'https://medium.com/js-testing-with-ava',
    tags: ['javascript', 'testing', 'ava'],
  });

  const user2 = await User.create({
    name: 'usern2',
    githubId: Date.now(),
  });

  const jwt = createUserJWT(user2, process.env.APP_SECRET);

  await request(app)
    .delete(`/api/links/${user1Link.id}`)
    .set('cookie', `daily-log-jwt=${jwt}`)
    .expect(401)
    .then((res, err) => {
      t.truthy(res.body.error);
      t.falsy(err);
    });
});
