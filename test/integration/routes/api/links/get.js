import test from 'ava';
import request from 'supertest';
import sinon from 'sinon';
import app from 'src/app';
import { Link, User } from 'src/models';
import createUserJWT from 'lib/auth/createUserJWT';
import { scaffold, teardown } from 'test/helpers';
import LinkFactory from 'test/helpers/factories/link';

const { mongoose } = scaffold();

test.after.always(() => teardown(mongoose));

test('guests cannot get links', async t => {
  await request(app)
    .get('/api/links')
    .expect(401)
    .then((res, err) => {
      t.falsy(err);

      t.is(res.body.statusCode, 401);
      t.is(res.body.error, 'Unauthorized');
    });
});

test('authenticated users can get their own links', async t => {
  const user = await User.create({
    name: 'user',
    githubId: Date.now(),
  });

  await Link.create(
    LinkFactory.buildList(3, {
      user: user.id,
    }),
  );

  const jwt = createUserJWT(user, process.env.APP_SECRET);

  await request(app)
    .get('/api/links')
    .set('cookie', `daily-log-jwt=${jwt}`)
    .expect(200)
    .then((res, err) => {
      t.is(res.body.length, 3);
      t.falsy(err);
    });
});

test('should handle internal server error', async t => {
  const user = await User.create({
    name: 'user',
    githubId: Date.now(),
  });

  sinon.stub(Link, 'find').rejects(new Error('internal'));

  const jwt = createUserJWT(user, process.env.APP_SECRET);

  await request(app)
    .get('/api/links')
    .set('cookie', `daily-log-jwt=${jwt}`)
    .expect(500)
    .then((res, err) => {
      t.truthy(res.body.error);
      t.falsy(err);

      Link.find.restore();
    });
});
