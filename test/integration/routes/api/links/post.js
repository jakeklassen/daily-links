import test from 'ava';
import request from 'supertest';
import shortid from 'shortid';
import sinon from 'sinon';
import nock from 'nock';
import { Error as MongooseError } from 'mongoose';
import app from 'src/app';
import { Link, User } from 'src/models';
import createUserJWT from 'lib/auth/createUserJWT';
import { scaffold, teardown } from 'test/helpers';

const { mongoose } = scaffold();

test.after.always(() => teardown(mongoose));

test('guests cannot create links', async t => {
  await request(app)
    .post('/api/links')
    .send({})
    .expect(401)
    .then((res, err) => {
      t.falsy(err);

      t.is(res.body.statusCode, 401);
      t.is(res.body.error, 'Unauthorized');
    });
});

test('authenticated users can create links', async t => {
  const user = await User.create({
    name: 'user',
    githubId: Date.now(),
  });

  const jwt = createUserJWT(user, process.env.APP_SECRET);
  const path = `/js-testing-with-ava-${shortid.generate()}`;

  nock('https://medium.com')
    .get(path)
    .reply(200, '<head><title>JS Testing With Ava</title></head>');

  const body = {
    url: `https://medium.com${path}`,
  };

  await request(app)
    .post('/api/links')
    .set('cookie', `daily-log-jwt=${jwt}`)
    .send(body)
    .expect(201)
    .then((res, err) => {
      t.truthy(res.body);
      t.is(res.body.title, 'JS Testing With Ava');
      t.falsy(err);
    });
});

test('should fail if url could not be found', async t => {
  const user = await User.create({
    name: 'user',
    githubId: Date.now(),
  });

  const path = `/js-testing-with-ava-${shortid.generate()}`;

  nock('https://medium.com')
    .get(path)
    .reply(404);

  const jwt = createUserJWT(user, process.env.APP_SECRET);

  await request(app)
    .post('/api/links')
    .set('cookie', `daily-log-jwt=${jwt}`)
    .send({
      url: `https://medium.com${path}`,
    })
    .expect(400)
    .then((res, err) => {
      t.is(res.body.statusCode, 400);
      t.truthy(res.body.error);
      t.falsy(err);
    });
});

test.serial('should handle internal server error', async t => {
  const user = await User.create({
    name: 'user',
    githubId: Date.now(),
  });

  sinon
    .stub(Link, 'create')
    .rejects(new MongooseError.ValidationError('internal'));

  const jwt = createUserJWT(user, process.env.APP_SECRET);

  const path = `/js-testing-with-ava-${shortid.generate()}`;

  nock('https://medium.com')
    .get(path)
    .reply(200, '<head><title>JS Testing With Ava</title></head>');

  await request(app)
    .post('/api/links')
    .set('cookie', `daily-log-jwt=${jwt}`)
    .send({
      url: `https://medium.com${path}`,
    })
    .expect(500)
    .then((res, err) => {
      t.truthy(res.body.error);
      t.falsy(err);

      Link.create.restore();
    });
});
