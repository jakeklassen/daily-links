import test from 'ava';
import request from 'supertest';
import sinon from 'sinon';
import app from 'src/app';
import { Link, User } from 'src/models';
import createUserJWT from 'lib/auth/createUserJWT';
import { scaffold, teardown } from 'test/helpers';
import LinkFactory from 'test/helpers/factories/link';

const { mongoose } = scaffold();

test.after.always(() => teardown(mongoose));

test('guests cannot patch links', async t => {
  const link = await Link.create(LinkFactory.build());

  await request(app)
    .patch(`/api/links/${link.id}`)
    .expect(401)
    .then((res, err) => {
      t.falsy(err);

      t.is(res.body.statusCode, 401);
      t.is(res.body.error, 'Unauthorized');
    });
});

test('authenticated users can patch their own links', async t => {
  const user = await User.create({
    name: 'user',
    githubId: Date.now(),
  });

  const link = await Link.create(
    LinkFactory.build({
      user: user.id,
    }),
  );

  const jwt = createUserJWT(user, process.env.APP_SECRET);

  await request(app)
    .patch(`/api/links/${link.id}`)
    .set('cookie', `daily-log-jwt=${jwt}`)
    .send({
      tags: ['testing'],
    })
    .expect(200)
    .then((res, err) => {
      t.falsy(err);
    });
});

test.serial('should handle internal server error', async t => {
  const user = await User.create({
    name: 'user',
    githubId: Date.now(),
  });

  const link = await Link.create(
    LinkFactory.build({
      user: user.id,
    }),
  );

  sinon.stub(Link, 'findByIdAndUpdate').rejects(new Error('internal'));

  const jwt = createUserJWT(user, process.env.APP_SECRET);

  await request(app)
    .patch(`/api/links/${link.id}`)
    .set('cookie', `daily-log-jwt=${jwt}`)
    .send({
      tags: ['testing'],
    })
    .expect(500)
    .then((res, err) => {
      t.truthy(res.body.error);
      t.falsy(err);

      Link.findByIdAndUpdate.restore();
    });
});
