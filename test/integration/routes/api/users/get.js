import test from 'ava';
import request from 'supertest';
import { xor } from 'lodash';
import app from 'src/app';
import { User } from 'src/models';
import createUserJWT from 'lib/auth/createUserJWT';
import { scaffold, teardown } from 'test/helpers';

const { mongoose } = scaffold();

test.after.always(() => teardown(mongoose));

test('guests cannot get user by any id', async t => {
  const user = await User.create({
    name: 'test',
    githubId: Date.now(),
  });

  await request(app)
    .get('/api/users/me')
    .expect(401)
    .then((res, err) => {
      t.falsy(err);

      t.is(res.body.statusCode, 401);
      t.is(res.body.error, 'Unauthorized');
    });

  await request(app)
    .get(`/api/users/${user.id}`)
    .expect(401)
    .then((res, err) => {
      t.falsy(err);

      t.is(res.body.statusCode, 401);
      t.is(res.body.error, 'Unauthorized');
    });
});

test('authenticated users can use special `me` user id', async t => {
  const user = await User.create({
    name: 'test',
    githubId: Date.now(),
  });

  const jwt = createUserJWT(user, process.env.APP_SECRET);

  await request(app)
    .get('/api/users/me')
    .set('cookie', `daily-log-jwt=${jwt}`)
    .expect(200)
    .then((res, err) => {
      t.falsy(err);
      t.deepEqual(
        xor(Object.keys(res.body), [
          'id',
          'name',
          'githubId',
          'createdAt',
          'updatedAt',
          'iat',
          'exp',
        ]),
        [],
      );
    });
});

test('authenticated users cannot get a user by id', async t => {
  const user = await User.create({
    name: 'test',
    githubId: Date.now(),
  });

  const user2 = await User.create({
    name: 'test2',
    githubId: Date.now(),
  });

  const jwt = createUserJWT(user, process.env.APP_SECRET);

  await request(app)
    .get(`/api/users/${user2.id}`)
    .set('cookie', `daily-log-jwt=${jwt}`)
    .expect(403)
    .then((res, err) => {
      t.falsy(err);

      t.is(res.body.statusCode, 403);
      t.is(res.body.error, 'Forbidden');
    });
});
