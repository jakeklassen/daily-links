import test from 'ava';
import request from 'supertest';
import app from 'src/app';
import { User } from 'src/models';
import createUserJWT from 'lib/auth/createUserJWT';
import { scaffold, teardown } from 'test/helpers';

const { mongoose } = scaffold();

test.after.always(async () => teardown(mongoose));

test('should redirect on request', t =>
  request(app)
    .get('/auth/github')
    .expect(302)
    .then(() => t.pass()));

test('should clear jwt cookie', async t => {
  const user = await User.create({
    name: 'test',
    githubId: Date.now(),
  });

  const jwt = createUserJWT(user, process.env.APP_SECRET);

  await request(app)
    .get('/auth/logout')
    .set('cookie', `daily-log-jwt=${jwt}`)
    .expect(302)
    .then((res, err) => {
      t.falsy(err);

      // Confirm we received a `set-cookie` header
      const [setCookie] = res.headers['set-cookie'];
      t.is(setCookie.split(';').shift(), 'daily-log-jwt=');
    });
});
