import fs from 'fs';
import test from 'ava';
import path from 'path';
import readHTMLHead from 'src/lib/url/readHTMLHead';

test('should only return <head> tag contents', async t => {
  const stream = fs.createReadStream(
    path.resolve(__dirname, '../../../helpers/fixtures/article.html'),
  );

  const head = await readHTMLHead(stream);

  t.true(head.startsWith('<head'));
  t.true(head.endsWith('</head>'));
});

test('should error on stream error', async t => {
  const stream = fs.createReadStream(
    path.resolve(__dirname, '../../../helpers/fixtures/article.html'),
  );

  process.nextTick(() => stream.emit('error', new Error('test')));

  await readHTMLHead(stream)
    .then(() => t.fail())
    .catch(() => t.pass());
});

test('should error on missing readable stream', async t => {
  await readHTMLHead().catch(() => t.pass());
});
